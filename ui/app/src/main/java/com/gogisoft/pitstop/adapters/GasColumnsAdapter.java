package com.gogisoft.pitstop.adapters;

import java.util.ArrayList;
import java.util.List;

import com.gogisoft.pitstop.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ArrayAdapter;

import com.gogisoft.pitstop.api_client.models.GasColumnModel;

public class GasColumnsAdapter extends ArrayAdapter<GasColumnModel> {

    public GasColumnsAdapter(Context context, List<GasColumnModel> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.gas_column_item, parent, false);
        }

        GasColumnModel item = getItem(position);

        TextView textView = (TextView)view.findViewById(R.id.gas_column_list_item_title);

        textView.setText(item.getName());

        return view;
    }
}
