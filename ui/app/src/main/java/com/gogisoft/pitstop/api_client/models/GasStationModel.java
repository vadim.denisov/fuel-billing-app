package com.gogisoft.pitstop.api_client.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * GasStation
 */
public class GasStationModel {
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("addr")
    @Expose
    private String addr;

    @SerializedName("lat")
    @Expose
    private Float lat;

    @SerializedName("lng")
    @Expose
    private Float lng;

    @SerializedName("height")
    @Expose
    private Float height;

    @SerializedName("columns")
    @Expose
    private List<GasColumnModel> columns = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public List<GasColumnModel> getColumns() {
        return columns;
    }

    public void setColumns(List<GasColumnModel> columns) {
        this.columns = columns;
    }
}
