package com.gogisoft.pitstop.adapters;

import java.util.ArrayList;
import java.util.List;

import com.gogisoft.pitstop.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MenuItemsAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<String> items;

    public MenuItemsAdapter(Context context, List<String> items) {
        this.context = context;
        this.items = items;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.menu_list_item, parent, false);
        }

        String item = this.items.get(position);

        TextView textView = (TextView)view.findViewById(R.id.menu_list_item_title);

        textView.setText(item);

        return view;
    }


}
