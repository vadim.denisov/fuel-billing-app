package com.gogisoft.pitstop.fragments;

import com.gogisoft.pitstop.api_client.models.GasStationModel;
import com.google.android.gms.maps.model.Marker;

import com.google.android.gms.maps.model.Marker;

import java.util.List;

import com.gogisoft.pitstop.App;
import com.gogisoft.pitstop.api_client.PitStopClient;
import com.gogisoft.pitstop.api_client.models.GasStationModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import android.location.Location;
import com.google.android.gms.location.LocationServices;

import com.google.android.gms.tasks.OnCompleteListener;

import android.content.pm.PackageManager;
import com.gogisoft.pitstop.R;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.GoogleMap.OnCameraMoveListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.gogisoft.pitstop.api_client.models.GasColumnModel;
import com.gogisoft.pitstop.adapters.GasColumnsAdapter;

import android.support.v4.app.ActivityCompat;

import android.support.v4.content.ContextCompat;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

public class Map extends Fragment implements OnMapReadyCallback, OnMarkerClickListener {
    private static final int DEFAULT_ZOOM = 15;

    private FusedLocationProviderClient mFusedLocationProviderClient;

    private GoogleMap mMap;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private boolean mLocationPermissionGranted;

    private View view;

    public MapView mMapView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)  {
        view = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView)view.findViewById(R.id.map);

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.getActivity());

        mMapView.onCreate(null);

        mMapView.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        mMap.setOnMarkerClickListener(this);

        getLocationPermission();

        updateLocationUI();

        getDeviceLocation();

        loadGasListAsync();
    }

    private void loadGasListAsync() {
        PitStopClient client = App.getPitStopClient();

        client.getGasstationModelList().enqueue(new Callback<List<GasStationModel>>() {
            @Override
            public void onResponse(Call<List<GasStationModel>> call, Response<List<GasStationModel>> response) {
                List<GasStationModel> list = response.body();

                for (GasStationModel item : list) {
                    createGasStationMarker(item);
                }
            }

            @Override
            public void onFailure(Call<List<GasStationModel>> call, Throwable t) {}
        });
    }

    private void createGasStationMarker(GasStationModel station) {
        mMap.addMarker(new MarkerOptions()
            .position(new LatLng(station.getLat(), station.getLng()))
            .title(station.getName())
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.gas_marker))
        ).setTag(station);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        GasStationModel gasStation = (GasStationModel)marker.getTag();

        App.ShowChoiceColumnDialog(
            this.getActivity(),
            gasStation
        );

        return false;
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this.getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;

        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this.getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            Location mLastKnownLocation = task.getResult();
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                        }
                    }
                });
            }
        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}
