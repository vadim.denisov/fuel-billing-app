package com.gogisoft.pitstop;


import android.os.Handler;
import android.os.SystemClock;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import android.view.LayoutInflater;
import android.app.Dialog;
import com.gogisoft.pitstop.databinding.PayDialogBinding;
import android.app.Activity;
import android.databinding.DataBindingUtil;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.ViewConfigurator;

import com.gogisoft.pitstop.adapters.FuelTypesAdapter;
import com.gogisoft.pitstop.adapters.GasColumnsAdapter;

import com.yarolegovich.lovelydialog.LovelyCustomDialog;

import com.gogisoft.pitstop.dialogs.PayDialogModel;
import com.gogisoft.pitstop.api_client.models.GasFuelTypeModel;
import com.gogisoft.pitstop.api_client.models.GasStationModel;

import android.app.Application;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;

import com.gogisoft.pitstop.api_client.PitStopClient;
import com.gogisoft.pitstop.api_client.models.GasColumnModel;
import com.yarolegovich.lovelydialog.LovelyChoiceDialog;

public class App extends Application {
    private Retrofit retrofit;
    private static PitStopClient client;

    @Override
    public void onCreate() {
        super.onCreate();
        this.buildPitStopClient();
    }

    private void buildPitStopClient() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
            .addInterceptor(new BasicAuthInterceptor("admin", "admin"))
            .build();

        retrofit = new Retrofit.Builder()
        .client(httpClient)
        .baseUrl("http://138.68.91.162/")
        .addConverterFactory(GsonConverterFactory.create())
        .build();

        client = retrofit.create(PitStopClient.class);
    }

    public static PitStopClient getPitStopClient() {
        return client;
    }

    public static void ShowChoiceColumnDialog(final Context context, final GasStationModel gasStation) {
        GasColumnsAdapter adapter = new GasColumnsAdapter(context, gasStation.getColumns());

        new LovelyChoiceDialog(context)
            .setTopColorRes(R.color.colorPrimary)
            .setTitle(gasStation.getName())
            .setIcon(R.drawable.gas_column)
            .setMessage(R.string.dialog_choose_column_message)
            .setItems(adapter, new LovelyChoiceDialog.OnItemSelectedListener<GasColumnModel>() {
                @Override
                public void onItemSelected(int position, GasColumnModel item) {
                    ShowChoiceFuelDialog(context, gasStation, item);
                }
            })
            .show();
    }

    public static void ShowChoiceFuelDialog(final Context context, final GasStationModel gasStation, final GasColumnModel gasColumn) {
        FuelTypesAdapter adapter = new FuelTypesAdapter(context, gasColumn.getFuelTypes());

        new LovelyChoiceDialog(context)
            .setTopColorRes(R.color.colorPrimary)
            .setTitle(gasStation.getName() + "->" + gasColumn.getName())
            .setIcon(R.drawable.gas_column)
            .setMessage("Выбирите тип топлива.")
            .setItems(adapter, new LovelyChoiceDialog.OnItemSelectedListener<GasFuelTypeModel>() {
                @Override
                public void onItemSelected(int position, GasFuelTypeModel item) {
                    ShowPayDialog(context, gasStation, gasColumn, item);
                }
            })
            .show();
    }

    public static void ShowPayDialog(final Context context, final GasStationModel gasStation, final GasColumnModel gasColumn, final GasFuelTypeModel gasFuel) {
        PayDialogBinding binding = DataBindingUtil
            .inflate(LayoutInflater.from(context), R.layout.pay_dialog, null, false);

        final PayDialogModel payDialogModel = new PayDialogModel(gasFuel.getPrice().toString());

        binding.setModel(payDialogModel);

        final LovelyCustomDialog dialog = new LovelyCustomDialog(context);

        dialog
            .setView(binding.getRoot())
            .setTopColorRes(R.color.colorPrimary)
            .setTitle(gasStation.getName() + "->" + gasColumn.getName() + "->" + gasFuel.getName())
            .setIcon(R.drawable.gas_column)
            .setMessage("Введите необходимое количество топлива, или цену.")
            .setListener(R.id.button_pay, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                    ShowProgressDialog(context, gasStation, gasColumn, gasFuel);
                }
            })
            .show();
    }

    public static void ShowProgressDialog(final Context context, final GasStationModel gasStation, final GasColumnModel gasColumn, final GasFuelTypeModel gasFuel) {
        final LovelyProgressDialog dialog = new LovelyProgressDialog(context);

        dialog
            .setTopColorRes(R.color.colorPrimary)
            .setIcon(R.drawable.gas_column)
            .setMessage("Пожалуйста подождите.")
            .setTitle(gasStation.getName() + "->" + gasColumn.getName() + "->" + gasFuel.getName())
            .show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();

                new LovelyInfoDialog(context)
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.gas_column)
                    .setTitle("Оплачено!")
                    .setMessage("Счастливого пути)")
                    .show();
            }
        }, 3000);
    }
}
