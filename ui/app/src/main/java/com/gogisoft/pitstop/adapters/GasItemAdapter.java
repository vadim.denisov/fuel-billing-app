package com.gogisoft.pitstop.adapters;

import java.util.ArrayList;
import java.util.List;

import com.gogisoft.pitstop.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gogisoft.pitstop.api_client.models.GasStationModel;

public class GasItemAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<GasStationModel> items;

    public GasItemAdapter(Context context, List<GasStationModel> items) {
        this.context = context;
        this.items = items;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.gas_list_item, parent, false);
        }

        GasStationModel item = this.items.get(position);

        TextView textViewName = (TextView)view.findViewById(R.id.gas_list_item_name);
        TextView textViewAddress = (TextView)view.findViewById(R.id.gas_list_item_address);

        textViewName.setText(item.getName());
        textViewAddress.setText(item.getAddr());

        return view;
    }


}
