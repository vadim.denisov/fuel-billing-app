package com.gogisoft.pitstop.dialogs;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

public class PayDialogModel extends BaseObservable {
    private String fuelPrice;
    private String price;
    private String count;

    public PayDialogModel(String fuelPrice) {
        setFuelPrice(fuelPrice);
        setCount("0");
    }

    @Bindable
    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        try {
            this.fuelPrice = fuelPrice;
            notifyPropertyChanged(com.gogisoft.pitstop.BR.fuelPrice);
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

    @Bindable
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        try {
            Float fuelPriceNum = new Float(this.fuelPrice);
            Float priceNum = new Float(price);

            this.price = price;
            this.count = new Float(priceNum / fuelPriceNum).toString();

            notifyPropertyChanged(com.gogisoft.pitstop.BR.price);
            notifyPropertyChanged(com.gogisoft.pitstop.BR.count);
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

    @Bindable
    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        try {
            Float fuelPriceNum = new Float(this.fuelPrice);
            Float countNum = new Float(count);

            this.count = count;
            this.price = new Float(countNum * fuelPriceNum).toString();

            notifyPropertyChanged(com.gogisoft.pitstop.BR.price);
            notifyPropertyChanged(com.gogisoft.pitstop.BR.count);
        } catch (Exception e) {
            //TODO: handle exception
        }

    }
 }
