package com.gogisoft.pitstop.api_client;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

import com.gogisoft.pitstop.api_client.models.GasStationModel;


// This class represent api for grafana: http://docs.grafana.org/http_api/
public interface PitStopClient {
    @GET("/api/gasstation/")
    Call<List<GasStationModel>> getGasstationModelList();
}
