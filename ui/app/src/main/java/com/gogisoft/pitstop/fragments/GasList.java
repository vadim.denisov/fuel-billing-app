package com.gogisoft.pitstop.fragments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.gogisoft.pitstop.App;
import com.gogisoft.pitstop.R;

import com.gogisoft.pitstop.adapters.GasColumnsAdapter;
import com.gogisoft.pitstop.adapters.GasItemAdapter;
import com.gogisoft.pitstop.api_client.PitStopClient;
import com.gogisoft.pitstop.api_client.models.GasColumnModel;
import com.gogisoft.pitstop.api_client.models.GasStationModel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.widget.AdapterView;
import android.content.Context;



public class GasList extends Fragment {
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)  {
        View view = inflater.inflate(R.layout.fragment_gas_list, container, false);

        listView = (ListView)view.findViewById(R.id.gas_list_view);

        loadGasListAsync();

        return view;
    }

    private void initGasList(final List<GasStationModel> items) {
        final Context activity = this.getActivity();

        GasItemAdapter adapter = new GasItemAdapter(this.getActivity(), items);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int index, long arg3) {
                GasStationModel gasStation = items.get(index);

                App.ShowChoiceColumnDialog(
                    activity,
                    gasStation
                );
            }
        });
    }

    private void loadGasListAsync() {
        PitStopClient client = App.getPitStopClient();

        client.getGasstationModelList().enqueue(new Callback<List<GasStationModel>>() {
            @Override
            public void onResponse(Call<List<GasStationModel>> call, Response<List<GasStationModel>> response) {
                List<GasStationModel> list = response.body();

                initGasList(list);
            }

            @Override
            public void onFailure(Call<List<GasStationModel>> call, Throwable t) {}
        });
    }
}
