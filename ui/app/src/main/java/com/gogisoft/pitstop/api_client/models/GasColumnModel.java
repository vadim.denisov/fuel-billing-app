package com.gogisoft.pitstop.api_client.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * GasStation
 */
public class GasColumnModel {
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("station")
    @Expose
    private Integer stationId;

    @SerializedName("fuel_type")
    @Expose
    private List<GasFuelTypeModel> fuelTypes = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public List<GasFuelTypeModel> getFuelTypes() {
        return fuelTypes;
    }

    public void setFuelTypes(List<GasFuelTypeModel> fuelTypes) {
        this.fuelTypes = fuelTypes;
    }
}
