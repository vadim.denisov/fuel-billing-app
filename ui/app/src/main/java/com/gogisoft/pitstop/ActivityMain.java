package com.gogisoft.pitstop;

import java.util.Arrays;
import java.util.List;

import android.content.res.Resources;
import java.util.ArrayList;

import java.util.List;

import android.view.View;

import com.mxn.soul.flowingdrawer_core.FlowingDrawer;

import android.widget.AdapterView;
import com.gogisoft.pitstop.adapters.MenuItemsAdapter;

import java.util.List;

import com.gogisoft.pitstop.R;
import com.gogisoft.pitstop.fragments.Map;
import com.gogisoft.pitstop.fragments.GasList;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ListView;
import android.content.Intent;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.widget.AdapterView.OnItemClickListener;


public class ActivityMain extends AppCompatActivity {

    private ListView menuItemsListView;
    private String currentMenuItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        menuItemsListView = (ListView)this.findViewById(R.id.menu_items_list_view);

        Resources res = getResources();

        List<String> menuItems = new ArrayList<String>();

        menuItems.addAll(Arrays.asList(res.getStringArray(R.array.menu_items)));

        updateMenuItemList(menuItems);

        drawFragmentById(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);
        return true;
    }

    private void updateMenuItemList(final List<String> list) {

        MenuItemsAdapter adapter = new MenuItemsAdapter(this, list);

        menuItemsListView.setAdapter(adapter);

        menuItemsListView.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int index, long arg3) {
                // Сорян, но мне похуй
                drawFragmentById(index);

                FlowingDrawer mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
                mDrawer.closeMenu();
            }
        });
    }

    private void drawFragmentById(int id) {
        Fragment fragment = null;

        switch (id) {
            case 0:
                fragment = (Fragment)new Map();
                break;
            case 1:
                fragment = (Fragment)new GasList();
                break;

            default:
                break;
        }

        if (fragment != null) {
            drawFragment(fragment);
        }
    }

    private void drawFragment(Fragment fragment) {
        FragmentTransaction fTrans = getFragmentManager().beginTransaction();

        fTrans.add(R.id.dashboard_container, fragment);
        fTrans.commit();
    }
}
