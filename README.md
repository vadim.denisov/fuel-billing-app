# Fuel Billing Application

## How to start developing

``` bash
source setenv.sh

# Create and activate virtualenv
virtualenv -p python3 venv
source venv/bin/activate

# Setup database
sudo -u postgres psql -c "CREATE ROLE $FB_API_DBUSER \
		WITH PASSWORD '$FB_API_DBPASS' \
		NOSUPERUSER CREATEDB NOCREATEROLE INHERIT LOGIN"
sudo -u postgres createdb $FB_API_DBNAME -O $FB_API_DBUSER


# Setup backend
cd backend
python setup.py develop

# Run api server
manage.py runserver
```

# Usage
``` bash
source setenv.sh
source venv/bin/activate
cd backend

# Run tests
manage.py test

# Apply fixture
manage.py loaddata backend/fbapi/gs/fixture/gs.json

# API Docs:
# Open backend/index.html on browser
# or
# Open swagger.yaml into https://editor.swagger.io/
```
