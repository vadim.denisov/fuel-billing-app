swagger: '2.0'
info:
  description: FBA Project API documentation
  version: "0.0.0"
  title: FBA
  contact:
    name: FBA Support
    email: q1munys@gmail.com
host: 127.0.0.1
basePath: /api
tags:
  - name: auth
    description: Authorization module
  - name: gs
    description: Access to gas stations
schemes:
  - http
security:
  - basic_auth: []
  - token_auth: []
paths:
  /auth/signup/:
    post:
      tags:
        - auth
      summary: Create new user
      security: []
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - username
              - email
              - password
              - confirm
            properties:
              username:
                type: string
              email:
                type: string
                format: email
              password:
                type: string
                format: password
              confirm:
                type: string
                format: password
              first_name:
                type: string
              last_name:
                type: string
      responses:
        '201':
          description: Created
          schema:
            $ref: '#/definitions/User'
        '400':
          description: Invalid input
  /auth/login/:
    post:
      tags:
        - auth
      summary: Create login token
      security: []
      parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            required:
              - username
              - password
            properties:
              username:
                type: string
              email:
                type: string
                format: email
              password:
                type: string
                format: password
      responses:
        '200':
          description: login success
          schema:
            type: object
            properties:
              key:
                type: string
        '400':
          description: Invalid input
  /auth/logout/:
    post:
      tags:
        - auth
      summary: Delete login token
      security: []
      responses:
        '200':
          description: 'logout success, returns success detail string'
          schema:
            type: object
            properties:
              detail:
                type: string
  /auth/me/:
    get:
      tags:
        - auth
      summary: Return current user
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/User'
    put:
      tags:
        - auth
      summary: Updates current user profile
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      security:
        - basic_auth: []
        - token_auth: []
      parameters:
        - in: body
          name: body
          required: true
          schema:
            $ref: '#/definitions/User'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/User'
        '400':
          description: Invalid input
  /gasstation:
    get:
      tags:
        - gs
      summary: Returns all gas stations
      responses:
        '200':
          description: successful operation
          schema:
            type: array
            items:
              $ref: '#/definitions/GasStation'
    post:
      tags:
        - gs
      summary: Created new gas stations
      parameters:
        - in: body
          name: body
          required: true
          schema:
            $ref: '#/definitions/GasStation'
      responses:
        '201':
          description: Created
          schema:
            $ref: '#/definitions/GasStation'
        '400':
          description: Invalid input
  /gasstation/{stationId}:
    get:
      tags:
        - gs
      summary: Get gas station by ID
      parameters:
        - name: stationId
          in: path
          required: true
          type: integer
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/GasStation'
        '404':
          description: Not found
    put:
      tags:
        - gs
      summary: Updates gas station with form data
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - name: stationId
          in: path
          required: true
          type: integer
          format: int64
        - name: name
          in: formData
          required: false
          type: string
          maxLength: 128
        - name: addr
          in: formData
          required: false
          type: string
          maxLength: 128
        - name: lat
          in: formData
          required: false
          type: number
          format: "float"
        - name: lng
          in: formData
          required: false
          type: number
          format: "float"
        - name: height
          in: formData
          required: false
          type: number
          format: "float"
      responses:
        '200':
          description: OK
        '400':
          description: Invalid input
    delete:
      tags:
        - gs
      summary: Deletes gas station
      parameters:
        - name: stationId
          in: path
          required: true
          type: integer
          format: int64
      responses:
        '204':
          description: No content
        '404':
          description: Not found
  /gascolumn:
    get:
      tags:
        - gs
      summary: Returns all gas columns
      responses:
        '200':
          description: successful operation
          schema:
            type: array
            items:
              $ref: '#/definitions/GasColumn'
    post:
      tags:
        - gs
      summary: Created new gas column
      parameters:
        - in: body
          name: body
          required: true
          schema:
            $ref: '#/definitions/GasColumn'
      responses:
        '201':
          description: Created
          schema:
            $ref: '#/definitions/GasColumn'
        '400':
          description: Invalid input
  /gascolumn/{columnId}:
    get:
      tags:
        - gs
      summary: Get gas station by ID
      parameters:
        - name: columnId
          in: path
          required: true
          type: integer
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/GasColumn'
        '404':
          description: Not found
    delete:
      tags:
        - gs
      summary: Deletes gas column
      parameters:
        - name: columnId
          in: path
          required: true
          type: integer
          format: int64
      responses:
        '204':
          description: No content
        '404':
          description: Not found
  /fueltype:
    get:
      tags:
        - gs
      summary: Returns all FuelTypes
      responses:
        '200':
          description: successful operation
          schema:
            type: array
            items:
              $ref: '#/definitions/FuelType'
    post:
      tags:
        - gs
      summary: Created new FuelType
      parameters:
        - in: body
          name: body
          required: true
          schema:
            $ref: '#/definitions/FuelType'
      responses:
        '201':
          description: Created
          schema:
            $ref: '#/definitions/FuelType'
        '400':
          description: Invalid input
  /fueltype/{fueltypeId}:
    get:
      tags:
        - gs
      summary: Get FuelType by ID
      parameters:
        - name: fueltypeId
          in: path
          required: true
          type: integer
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/FuelType'
        '404':
          description: Not found
    delete:
      tags:
        - gs
      summary: Deletes gas column
      parameters:
        - name: fueltypeId
          in: path
          required: true
          type: integer
          format: int64
      responses:
        '204':
          description: No content
        '404':
          description: Not found
securityDefinitions:
  basic_auth:
    type: basic
    description: Basic Authorization Mechanism
  token_auth:
    type: apiKey
    in: header
    name: Token
    description: Authorization Token Mechanism
definitions:
  User:
    type: object
    required:
      - username
      - email
      - password
    properties:
      id:
        type: number
        format: int64
        example: 1
      username:
        type: string
        minLength: 4
        maxLength: 64
        example: iam.denzel
      email:
        type: string
        format: email
        maxLength: 64
        example: email@gmail.com
      first_name:
        type: string
        maxLength: 64
        example: Denzel
      last_name:
        type: string
        maxLength: 64
        example: Washington
      password:
        type: string
        minLength: 4
        maxLength: 64
        example: secretpassw
  GasStation:
    type: object
    required:
      - name
      - addr
      - lat
      - lng
      - height
    properties:
      id:
        type: number
        format: int64
        example: 1
      name:
        type: string
        maxLength: 128
        example: "Fuel Station #1"
      addr:
        type: string
        maxLength: 128
        example: "Minsk, st. Maksima Tanka 9"
      lat:
        type: number
        format: "float"
        example: 41.0
      lng:
        type: number
        format: "float"
        example: 13.0
      height:
        type: number
        format: "float"
        example: 12.0
      user:
        $ref: '#/definitions/User'
  GasColumn:
    type: object
    required:
      - name
      - fuel_type
      - station
    properties:
      id:
        type: number
        format: int64
        example: 1
      name:
        type: string
        maxLength: 128
        example: "Fuel Station #1"
      fuel_type:
        type: string
        maxLength: 128
        example: "Minsk, st. Maksima Tanka 9"
      station:
        $ref: '#/definitions/GasStation'
  FuelType:
    type: object
    required:
      - name
      - price
    properties:
      id:
        type: number
        format: int64
        example: 1
      name:
        type: string
        maxLength: 128
        example: "Fuel Station #1"
      price:
        type: number
        format: "float"
        example: 2.3
        