import os
import unittest
from setuptools import setup, find_packages


def test_suite():
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover(
        'fb', pattern='test.py', top_level_dir='.')
    return test_suite


setup(
    name='fb',
    version=os.environ.get('FB_VERSION', '0.0.0'),
    install_requires=[
        'django==2.1.5',
        'django-rest-auth==0.9.3',
        'djangorestframework==3.8.2',
        'factory-boy==2.11.1',
        'flake8==3.6.0',
        'psycopg2==2.7.6.1',
        'requests==2.21.0',
    ],
    packages=find_packages(),
    test_suite='setup.test_suite',
    entry_points={
        'console_scripts': [
            'manage.py = fbapi.manage:main',
            'jobd = jobd.__main__:main',
        ],
    },
)
