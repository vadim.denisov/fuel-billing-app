from rest_framework import generics
from rest_auth.views import (
    UserDetailsView as DefaultUserDetailsView,
)
from users.serializers import (
    UsersSerializer, SignUpSerializer,
)


class SignUpView(generics.CreateAPIView):
    serializer_class = SignUpSerializer
    permission_classes = []


class UserDetailsView(DefaultUserDetailsView):
    serializer_class = UsersSerializer
