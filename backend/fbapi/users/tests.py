import factory

from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from users.models import User
from users.factories import UserFactory


class UserTestCase(APITestCase):
    USER_FIELDS = ('username', 'email', 'first_name', 'last_name')

    def test_signup(self):
        data = factory.build(dict, FACTORY_CLASS=UserFactory)
        data['confirm'] = data['password']

        response = self.client.post('/api/auth/signup/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        for field in self.USER_FIELDS:
            self.assertEqual(response.data[field], data[field],
                             'invalid `{}` in response'.format(field))

        actual = User.objects.values()[0]
        for field in self.USER_FIELDS:
            self.assertEqual(actual[field], data[field],
                             'invalid `{}` in db'.format(field))

    def test_signup_double_username(self):
        first = UserFactory()
        data = factory.build(dict, FACTORY_CLASS=UserFactory,
                             username=first.username)
        data['confirm'] = data['password']

        response = self.client.post('/api/auth/signup/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_signup_double_email(self):
        first = UserFactory()
        data = factory.build(dict, FACTORY_CLASS=UserFactory,
                             email=first.email)
        data['confirm'] = data['password']

        response = self.client.post('/api/auth/signup/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_signup_invalid_confirm(self):
        data = factory.build(dict, FACTORY_CLASS=UserFactory)
        data['confirm'] = 'invalid'

        response = self.client.post('/api/auth/signup/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login(self):
        password = 'password'
        user = UserFactory(password=password)

        data = {'username': user.username, 'password': password}
        response = self.client.post('/api/auth/login/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        token = Token.objects.get()
        self.assertEqual(token.key, response.data['key'])

    def test_login_invalid(self):
        user = UserFactory(password='password')

        cases = (
            {'username': user.username, 'password': 'invalid'},
            {'username': 'invalid', 'password': 'password'},
        )
        for data in cases:
            response = self.client.post('/api/auth/login/', data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

            self.assertFalse(Token.objects.exists())

    def test_logout(self):
        user = UserFactory()

        self.client.force_login(user)

        response = self.client.post('/api/auth/logout/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertFalse(Token.objects.exists())

    def test_invalid_logout(self):
        response = self.client.post('/api/auth/logout/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_me(self):
        data = factory.build(dict, FACTORY_CLASS=UserFactory)
        user = UserFactory(**data)

        self.client.force_login(user)

        response = self.client.get('/api/auth/me/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        for field in self.USER_FIELDS:
            self.assertEqual(response.data[field], data[field],
                             'invalid `{}` in response'.format(field))

    def test_me_not_login(self):
        UserFactory()

        response = self.client.get('/api/auth/me/', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_me_update(self):
        first_name = 'first_name'
        user = UserFactory(first_name=first_name + '_invalid')

        self.client.force_login(user)

        data = {'first_name': first_name}
        response = self.client.patch('/api/auth/me/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user = User.objects.get(id=user.id)
        self.assertEqual(user.first_name, first_name)
