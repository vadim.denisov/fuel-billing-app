from django.conf.urls import url
from rest_auth.views import LoginView, LogoutView
from users.views import SignUpView, UserDetailsView

urlpatterns = [
    url(r'^auth/signup/$', SignUpView.as_view(), name='signup'),
    url(r'^auth/login/$', LoginView.as_view(), name='login'),
    url(r'^auth/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^auth/me/$', UserDetailsView.as_view(), name='user_details'),
]
