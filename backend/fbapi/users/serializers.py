from rest_framework import serializers
from users.models import User


class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'username', 'email',
            'first_name', 'last_name', )
        read_only_fields = ('username', )


class SignUpSerializer(serializers.ModelSerializer):
    password = serializers.CharField(min_length=6, max_length=64,
                                     required=True, write_only=True)
    confirm = serializers.CharField(
        max_length=64, required=True, write_only=True)
    is_active = serializers.BooleanField(default=False)

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name',
                  'password', 'confirm', 'is_active')

    def validate_username(self, username):
        username = username.lower()
        if User.objects.filter(username=username).exists():
            raise serializers.ValidationError(
                "A user is already registered with this username.")
        return username

    def validate_email(self, email):
        email = User.objects.normalize_email(email)
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError(
                "A user is already registered with this e-mail address.")
        return email

    def validate(self, data):
        if data['password'] != data['confirm']:
            raise serializers.ValidationError(
                "The confirm field didn't match.")
        data.pop('confirm')
        return data

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    def get_user_obj(self):
        return User(**self.validated_data)
