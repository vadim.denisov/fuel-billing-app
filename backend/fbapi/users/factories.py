import factory
import factory.fuzzy
from users.models import User


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    password = factory.fuzzy.FuzzyText()
    first_name = factory.fuzzy.FuzzyText('first_name', length=4)
    last_name = factory.fuzzy.FuzzyText('last_name', length=4)

    @factory.sequence
    def username(n):
        return 'user{}'.format(n)

    @factory.lazy_attribute
    def email(self):
        return '{}@example.com'.format(self.username)

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        manager = cls._get_manager(model_class)
        return manager.create_user(*args, **kwargs)
