from rest_framework import serializers
from gs.models import (
    GasStation, GasColumn, FuelType
)


class FuelTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FuelType
        fields = ('id', 'name', 'price', )


class GasColumnSerializer(serializers.ModelSerializer):
    fuel_type = FuelTypeSerializer(many=True, read_only=True)

    class Meta:
        model = GasColumn
        fields = ('id', 'name', 'fuel_type',
                  'station', )


class GasStationSerializer(serializers.ModelSerializer):
    columns = GasColumnSerializer(many=True, read_only=True)

    class Meta:
        model = GasStation
        fields = (
            'id', 'name', 'addr',
            'lat', 'lng', 'height', 'columns')
