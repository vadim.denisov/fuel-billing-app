# Generated by Django 2.1.5 on 2019-01-19 19:48

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gs', '0004_auto_20190119_1943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gasstation',
            name='users',
            field=models.ManyToManyField(blank=True, related_name='favorites', to=settings.AUTH_USER_MODEL),
        ),
    ]
