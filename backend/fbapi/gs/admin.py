from django.contrib import admin
from gs.models import GasStation, GasColumn, FuelType


@admin.register(GasStation)
class GasStationAdmin(admin.ModelAdmin):
    fields = ('name', 'addr',
              'lat', 'lng', 'height', 'users')


@admin.register(GasColumn)
class GasColumnAdmin(admin.ModelAdmin):
    fields = ('name', 'fuel_type', 'station')


@admin.register(FuelType)
class FuelTypeAdmin(admin.ModelAdmin):
    fields = ('name', 'price')
