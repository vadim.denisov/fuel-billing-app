from gs.views import (
    GasStationViewSet, GasColumnViewSet,
    FuelTypeViewSet,
)
from rest_framework import routers


router = routers.SimpleRouter()
router.register(r'gasstation', GasStationViewSet)
router.register(r'gascolumn', GasColumnViewSet)
router.register(r'fueltype', FuelTypeViewSet)

urlpatterns = router.urls
