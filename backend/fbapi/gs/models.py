from django.db import models
from users.models import User


class GasStation(models.Model):
    name = models.CharField(max_length=128)
    addr = models.CharField(max_length=128)

    lat = models.FloatField()
    lng = models.FloatField()
    height = models.FloatField()

    users = models.ManyToManyField(User, related_name='favorites', blank=True)

    def __str__(self):
        return '{}, {}'.format(self.name, self.addr)


class GasColumn(models.Model):
    name = models.CharField(max_length=128)
    fuel_type = models.ManyToManyField('FuelType')
    station = models.ForeignKey('GasStation', related_name='columns', on_delete=models.CASCADE)

    def __str__(self):
        return 'Column {}, {}'.format(self.name, self.station)


class FuelType(models.Model):
    name = models.CharField(max_length=128, unique=True)
    price = models.FloatField()

    def __str__(self):
        return '{} -- {}'.format(self.price, self.name)
