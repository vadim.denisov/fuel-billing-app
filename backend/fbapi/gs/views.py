from rest_framework.viewsets import ModelViewSet
from gs.models import (
    GasStation, GasColumn, FuelType
)
from gs.serializers import (
    GasStationSerializer, GasColumnSerializer,
    FuelTypeSerializer,
)


class GasStationViewSet(ModelViewSet):
    queryset = GasStation.objects.all()
    serializer_class = GasStationSerializer


class GasColumnViewSet(ModelViewSet):
    queryset = GasColumn.objects.all()
    serializer_class = GasColumnSerializer


class FuelTypeViewSet(ModelViewSet):
    queryset = FuelType.objects.all()
    serializer_class = FuelTypeSerializer
