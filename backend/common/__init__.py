import logging

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S.%f'


def _parse_logging_level(level):
    numeric_level = getattr(logging, level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: {}'.format(level))
    return numeric_level


def init_logging(level='info'):
    level = _parse_logging_level(level)
    logging.basicConfig(
        format='[%(name)s]: %(levelname)s: %(message)s',
        level=level,
    )


class IJob(object):
    __slots__ = ()

    def run(self, ctx):
        raise NotImplementedError('must be implemented')
