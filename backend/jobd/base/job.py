class Context(object):
    __slots__ = ('now')

    def __init__(self, now=None):
        self.now = now


class IJob(object):
    __slots__ = ()

    def run(self, ctx):
        raise NotImplementedError('must be implemented')
