import time
import datetime
import logging

from .base import Context

logger = logging.getLogger('jobd')


def utcnow():
    ''' This func is mocked in tests.'''
    return datetime.datetime.utcnow().replace(
        tzinfo=datetime.timezone.utc,
    )


class Daemon(object):
    __slots__ = ('ctx', 'jobs',)

    _wait_timeout = 30

    def __init__(self, jobs):
        self.jobs = jobs

        self.ctx = Context()

    @classmethod
    def set_wait_timeout(cls, timeout):
        cls._wait_timeout = timeout

    def run(self, niter=(1 << 64) - 1):
        logger.info('daemon: start...')

        ctx = self.ctx
        for _ in range(niter):
            ctx.now = utcnow()
            for job in self.jobs:
                job.run(ctx)

            logger.info('daemon: sleep {} sec...'.format(
                self._wait_timeout))
            time.sleep(self._wait_timeout)
