import logging

from common import init_logging
from jobd.price_setter.price_setter import PriceSetter
from jobd.daemon import Daemon


logger = logging.getLogger('jobd')


def main():
    init_logging()

    daemon = Daemon(
        jobs=(PriceSetter(), ))

    try:
        daemon.run()
    except Exception as e:
        logger.exception('main: {}'.format(e))
