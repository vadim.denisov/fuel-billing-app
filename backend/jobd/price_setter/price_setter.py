import logging
import requests
import lxml.html as html

from common import IJob
from gs.models import FuelType


logger = logging.getLogger('jobd: PriceSetter: ')


class PriceSetter(IJob):
    __slots__ = ('url')

    def __init__(self):
        self.url = 'https://azs.belorusneft.by/sitebeloil/ru/center/azs/center/fuelandService/price/'

    def run(self, ctx):
        logger.info('update: start...')

        page = requests.get(self.url)
        tree = html.fromstring(page.content)
        table = tree.xpath('//*[@id="leftcontainer"]/div/div/table')[0]
        raw_data = [ch.text_content()
                    for tr in table.getchildren()
                    for ch in tr.getchildren()]
        FuelType.objects.filter(name=raw_data[0]).update(price=float(raw_data[2]))
        FuelType.objects.filter(name=raw_data[1]).update(price=float(raw_data[3]))
        FuelType.objects.filter(name=raw_data[4]).update(price=float(raw_data[6]))
        FuelType.objects.filter(name=raw_data[5][:-1]).update(price=float(raw_data[7]))
        FuelType.objects.filter(name=raw_data[8]).update(price=float(raw_data[10]))
        FuelType.objects.filter(name=raw_data[9][:-5]).update(price=float(raw_data[11]))
